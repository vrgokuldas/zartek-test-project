import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_zartek/providers/provider_cart.dart';
import 'package:test_zartek/providers/provider_restaurent.dart';
import 'package:test_zartek/widgets/badge.dart';
import 'package:test_zartek/widgets/menu_list_body.dart';

class ScreenHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final repo = Provider.of<ProviderRestaurent>(context,listen: true);
    final cartRepo=Provider.of<ProviderCart>(context,listen: true);
    final themeColor = Theme.of(context).primaryColor;
    print(repo.isLoading);
    return repo.isLoading
        ? Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : DefaultTabController(
            length: repo.CategoriesList.length,
            child: Scaffold(
              appBar: AppBar(
                title: Text(
                  repo.RestaurentName,
                  style: TextStyle(color: Colors.black),
                ),
                bottom: TabBar(
                  isScrollable: true,
                  tabs: repo.CategoriesList.map<Widget>(
                      (String categoryName) => Tab(
                            text: categoryName,
                          )).toList(),
                  indicatorColor: themeColor,
                  labelColor: themeColor,
                  unselectedLabelColor: Colors.grey,
                ),
                actions: <Widget>[
                  Center(
                    child: Text(
                      'My Orders',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Badge(
                    child: IconButton(
                      icon: Icon(
                        Icons.shopping_cart,
                        color: Colors.grey,
                      ),
                      onPressed: () {},
                    ),
                    value: cartRepo.ItemCount.toString(),
                    color: Colors.red,
                  )
                ],
                backgroundColor: Colors.white,
              ),
              body: TabBarView(
                children: repo.CategoriesList.map<Widget>((category) {
                  return MenuListBody(context, category);
                }).toList(),
              ),
            ),
          );
  }
}
