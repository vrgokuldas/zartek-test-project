import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_zartek/models/model_items.dart';
import 'package:test_zartek/providers/provider_cart.dart';

class MenuSingleItem extends StatelessWidget {
  final ModelItems foodItem;
  final vegSticker =
      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQUb3BqiMpvvYFm8Wy6iK3TyLmePhXi7HtmaqOnEkL2rQYJ73qV';
  final nonVegSticker =
      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSR0zx3UuEHlgKKWctBxSGfxJUdm3AgWcV2QSPHGbOjzla4AOeN';
  final eggSticker =
      'https://pngimage.net/wp-content/uploads/2018/05/egg-icon-png-6.png';
  final noImageAvailable =
      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS2CTZfk190l-eimWvKPZTkWgHKVA8McQguQ0fZZDJ9ljguDAhD';
  ProviderCart cartRepo;

  MenuSingleItem(this.foodItem);

  @override
  Widget build(BuildContext context) {
    cartRepo = Provider.of<ProviderCart>(context);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _foodTypeSticker,
              SizedBox(width: 8),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    _headinng,
                    SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          _foodPrice,
                          _foodCalories,
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    _foodDescription,
                    SizedBox(
                      height: 8,
                    ),
                    CartManager,
                    SizedBox(
                      height: 8,
                    ),
                    if (foodItem.isAddonAvailable)
                      Text(
                        "Customizations Available",
                        style: TextStyle(color: Colors.red, fontSize: 14),
                      )
                  ],
                ),
              ),
              _foodImage
            ],
          ),
          Divider()
        ],
      ),
    );
  }
  /*
    * Using to add and remove items from cart
    */
  Widget get CartManager {
    return Container(
      width: 130,
      height: 35,
      decoration: BoxDecoration(
        color: Colors.green,
        borderRadius: BorderRadius.circular(30),
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 8, right: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.remove,
                color: Colors.white,
              ),
              onPressed: ()=>cartRepo.removeSingleItem(foodItem.id),
            ),
            Text(
              foodItem.orderCount.toString(),
              style: TextStyle(color: Colors.white),
            ),
            IconButton(
              icon: Icon(Icons.add, color: Colors.white),
              onPressed: ()=>cartRepo.addItem(foodItem),
            )
          ],
        ),
      ),
    );
  }
  /*
    * returns url of veg, non-veg, egg food stickers WRT food type
    */
  String get foodStickerUrl {
    if (foodItem.foodType == 1) {
      return nonVegSticker;
    } else if (foodItem.foodType == 2) {
      return vegSticker;
    } else {
      return eggSticker;
    }
  }
  /*
    * for showing its veg, non-veg, or egg
    */
  Widget get _foodTypeSticker {
    return Container(
        height: 20,
        width: 20,
        child: Image.network(
          foodStickerUrl,
          fit: BoxFit.cover,
        ));
  }
  /*
    * food item name
    */
  Widget get _headinng {
    return Text(
      foodItem.name,
      maxLines: 1,
      style: TextStyle(
        fontSize: 15,
        fontWeight: FontWeight.bold,
      ),
    );
  }
/*
    * food item price
    */
  Widget get _foodPrice {
    return Text(
      '${foodItem.price} ${foodItem.currencyType}',
      style: TextStyle(
        fontWeight: FontWeight.bold,
      ),
    );
  }
  /*
    * food item calories
    */
  Widget get _foodCalories {
    return Text(
      '${foodItem.calories} Calories',
      style: TextStyle(
        fontWeight: FontWeight.bold,
      ),
    );
  }
  /*
    * food item description
    */
  Widget get _foodDescription {
    return Text(
      foodItem.description,
      style: TextStyle(color: Colors.grey),
    );
  }
    /*
    * food item image on right
    */
  Widget get _foodImage {
    //print('${foodItem.menuCategory} ${foodItem.imageUrl}');
    final imageUrl =
        foodItem.imageUrl != "" ? foodItem.imageUrl : noImageAvailable;
    return Container(
      height: 100,
      width: 100,
      child: Image.network(
        imageUrl,
        fit: BoxFit.cover,
      ),
    );
  }
}
