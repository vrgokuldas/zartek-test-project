import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_zartek/models/model_items.dart';
import 'package:test_zartek/providers/provider_cart.dart';
import 'package:test_zartek/providers/provider_restaurent.dart';
import 'package:test_zartek/widgets/menu_single_item.dart';

class MenuListBody extends StatelessWidget {
  final BuildContext context;
  final String food_category;

  MenuListBody(this.context, this.food_category);

  @override
  Widget build(BuildContext context) {
    /*
    * Using food category, we will fetch the only required food items from the json
    */
    final foodList = Provider.of<ProviderRestaurent>(context, listen: false).OptimizedFoodList(food_category);
    /*
    * cartRepo used to compare items in cart and item displayed in screen
    */
    final cartRepo = Provider.of<ProviderCart>(context, listen: true);
    /*
    * after each comparison, items will add to this new List
    */
    List<ModelItems> newList = new List<ModelItems>();
    /*
    * Comparing each item with cart item
    */
    foodList.forEach((singleFood) {
      newList.add(ModelItems(
          id: singleFood.id,
          menuCategory: singleFood.menuCategory,
          name: singleFood.menuCategory,
          price: singleFood.price,
          currencyType: singleFood.currencyType,
          calories: singleFood.calories,
          description: singleFood.description,
          isAddonAvailable: singleFood.isAddonAvailable,
          imageUrl: singleFood.imageUrl,
          foodType: singleFood.foodType,
          orderCount: cartRepo.getItemCountFromCart(singleFood.id)));
    });
    /*
    * After comparing, updated items and old items will show in the UI
    */
    return ListView.builder(
        itemCount: newList.length,
        itemBuilder: (ctx, index) {
          return MenuSingleItem(newList[index]);
        });
  }
}
