import 'package:flutter/material.dart';
import 'package:test_zartek/models/model_items.dart';

class ModelCartItem {
  final ModelItems item;
  final int quantity;
  ModelCartItem({@required this.item, @required this.quantity});
}
