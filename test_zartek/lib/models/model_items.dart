class ModelItems
{
  final String id;
  final String menuCategory;
  final String name;
  final String price;
  final String currencyType;
  final String calories;
  final String description;
  final bool isAddonAvailable;
  final String imageUrl;
  final int foodType;
  final int orderCount;

  ModelItems({this.id, this.menuCategory, this.name, this.price,
      this.currencyType, this.calories, this.description, this.isAddonAvailable,
      this.imageUrl,this.foodType,this.orderCount=0});

}