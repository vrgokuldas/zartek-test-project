class ModelRestaurentPool{
  List<ModelRestaurent>  restaurentList;

  ModelRestaurentPool(this.restaurentList);

  factory ModelRestaurentPool.fromJson(List<dynamic> jsonString)
  {
    List<ModelRestaurent>  _restaurentList= new List<ModelRestaurent>();
    _restaurentList=jsonString.map((item)=>ModelRestaurent.fromJson(item)).toList();
    return ModelRestaurentPool(_restaurentList);
  }
}

class ModelRestaurent {
  String _restaurantId;
  String _restaurantName;
  String _restaurantImage;
  String _tableId;
  String _tableName;
  String _branchName;
  String _nexturl;
  List<TableMenuList> _tableMenuList;

  ModelRestaurent(
      {String restaurantId,
        String restaurantName,
        String restaurantImage,
        String tableId,
        String tableName,
        String branchName,
        String nexturl,
        List<TableMenuList> tableMenuList}) {
    this._restaurantId = restaurantId;
    this._restaurantName = restaurantName;
    this._restaurantImage = restaurantImage;
    this._tableId = tableId;
    this._tableName = tableName;
    this._branchName = branchName;
    this._nexturl = nexturl;
    this._tableMenuList = tableMenuList;
  }

  String get restaurantId => _restaurantId;
  set restaurantId(String restaurantId) => _restaurantId = restaurantId;
  String get restaurantName => _restaurantName;
  set restaurantName(String restaurantName) => _restaurantName = restaurantName;
  String get restaurantImage => _restaurantImage;
  set restaurantImage(String restaurantImage) =>
      _restaurantImage = restaurantImage;
  String get tableId => _tableId;
  set tableId(String tableId) => _tableId = tableId;
  String get tableName => _tableName;
  set tableName(String tableName) => _tableName = tableName;
  String get branchName => _branchName;
  set branchName(String branchName) => _branchName = branchName;
  String get nexturl => _nexturl;
  set nexturl(String nexturl) => _nexturl = nexturl;
  List<TableMenuList> get tableMenuList => _tableMenuList;
  set tableMenuList(List<TableMenuList> tableMenuList) =>
      _tableMenuList = tableMenuList;

  ModelRestaurent.fromJson(Map<String, dynamic> json) {
    _restaurantId = json['restaurant_id'];
    _restaurantName = json['restaurant_name'];
    _restaurantImage = json['restaurant_image'];
    _tableId = json['table_id'];
    _tableName = json['table_name'];
    _branchName = json['branch_name'];
    _nexturl = json['nexturl'];
    if (json['table_menu_list'] != null) {
      _tableMenuList = new List<TableMenuList>();
      json['table_menu_list'].forEach((v) {
        _tableMenuList.add(new TableMenuList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['restaurant_id'] = this._restaurantId;
    data['restaurant_name'] = this._restaurantName;
    data['restaurant_image'] = this._restaurantImage;
    data['table_id'] = this._tableId;
    data['table_name'] = this._tableName;
    data['branch_name'] = this._branchName;
    data['nexturl'] = this._nexturl;
    if (this._tableMenuList != null) {
      data['table_menu_list'] =
          this._tableMenuList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TableMenuList {
  String _menuCategory;
  String _menuCategoryId;
  String _menuCategoryImage;
  String _nexturl;
  List<CategoryDishes> _categoryDishes;

  TableMenuList(
      {String menuCategory,
        String menuCategoryId,
        String menuCategoryImage,
        String nexturl,
        List<CategoryDishes> categoryDishes}) {
    this._menuCategory = menuCategory;
    this._menuCategoryId = menuCategoryId;
    this._menuCategoryImage = menuCategoryImage;
    this._nexturl = nexturl;
    this._categoryDishes = categoryDishes;
  }

  String get menuCategory => _menuCategory;
  set menuCategory(String menuCategory) => _menuCategory = menuCategory;
  String get menuCategoryId => _menuCategoryId;
  set menuCategoryId(String menuCategoryId) => _menuCategoryId = menuCategoryId;
  String get menuCategoryImage => _menuCategoryImage;
  set menuCategoryImage(String menuCategoryImage) =>
      _menuCategoryImage = menuCategoryImage;
  String get nexturl => _nexturl;
  set nexturl(String nexturl) => _nexturl = nexturl;
  List<CategoryDishes> get categoryDishes => _categoryDishes;
  set categoryDishes(List<CategoryDishes> categoryDishes) =>
      _categoryDishes = categoryDishes;

  TableMenuList.fromJson(Map<String, dynamic> json) {
    _menuCategory = json['menu_category'];
    _menuCategoryId = json['menu_category_id'];
    _menuCategoryImage = json['menu_category_image'];
    _nexturl = json['nexturl'];
    if (json['category_dishes'] != null) {
      _categoryDishes = new List<CategoryDishes>();
      json['category_dishes'].forEach((v) {
        _categoryDishes.add(new CategoryDishes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['menu_category'] = this._menuCategory;
    data['menu_category_id'] = this._menuCategoryId;
    data['menu_category_image'] = this._menuCategoryImage;
    data['nexturl'] = this._nexturl;
    if (this._categoryDishes != null) {
      data['category_dishes'] =
          this._categoryDishes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CategoryDishes {
  String _dishId;
  String _dishName;
  double _dishPrice;
  String _dishImage;
  String _dishCurrency;
  double _dishCalories;
  String _dishDescription;
  bool _dishAvailability;
  int _dishType;
  String _nexturl;
  List<AddonCat> _addonCat;

  CategoryDishes(
      {String dishId,
        String dishName,
        double dishPrice,
        String dishImage,
        String dishCurrency,
        double dishCalories,
        String dishDescription,
        bool dishAvailability,
        int dishType,
        String nexturl,
        List<AddonCat> addonCat}) {
    this._dishId = dishId;
    this._dishName = dishName;
    this._dishPrice = dishPrice;
    this._dishImage = dishImage;
    this._dishCurrency = dishCurrency;
    this._dishCalories = dishCalories;
    this._dishDescription = dishDescription;
    this._dishAvailability = dishAvailability;
    this._dishType = dishType;
    this._nexturl = nexturl;
    this._addonCat = addonCat;
  }

  String get dishId => _dishId;
  set dishId(String dishId) => _dishId = dishId;
  String get dishName => _dishName;
  set dishName(String dishName) => _dishName = dishName;
  double get dishPrice => _dishPrice;
  set dishPrice(double dishPrice) => _dishPrice = dishPrice;
  String get dishImage => _dishImage;
  set dishImage(String dishImage) => _dishImage = dishImage;
  String get dishCurrency => _dishCurrency;
  set dishCurrency(String dishCurrency) => _dishCurrency = dishCurrency;
  double get dishCalories => _dishCalories;
  set dishCalories(double dishCalories) => _dishCalories = dishCalories;
  String get dishDescription => _dishDescription;
  set dishDescription(String dishDescription) =>
      _dishDescription = dishDescription;
  bool get dishAvailability => _dishAvailability;
  set dishAvailability(bool dishAvailability) =>
      _dishAvailability = dishAvailability;
  int get dishType => _dishType;
  set dishType(int dishType) => _dishType = dishType;
  String get nexturl => _nexturl;
  set nexturl(String nexturl) => _nexturl = nexturl;
  List<AddonCat> get addonCat => _addonCat;
  set addonCat(List<AddonCat> addonCat) => _addonCat = addonCat;

  CategoryDishes.fromJson(Map<String, dynamic> json) {
    _dishId = json['dish_id'];
    _dishName = json['dish_name'];
    _dishPrice = json['dish_price'];
    _dishImage = json['dish_image'];
    _dishCurrency = json['dish_currency'];
    _dishCalories = json['dish_calories'];
    _dishDescription = json['dish_description'];
    _dishAvailability = json['dish_Availability'];
    _dishType = json['dish_Type'];
    _nexturl = json['nexturl'];
    if (json['addonCat'] != null) {
      _addonCat = new List<AddonCat>();
      json['addonCat'].forEach((v) {
        _addonCat.add(new AddonCat.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dish_id'] = this._dishId;
    data['dish_name'] = this._dishName;
    data['dish_price'] = this._dishPrice;
    data['dish_image'] = this._dishImage;
    data['dish_currency'] = this._dishCurrency;
    data['dish_calories'] = this._dishCalories;
    data['dish_description'] = this._dishDescription;
    data['dish_Availability'] = this._dishAvailability;
    data['dish_Type'] = this._dishType;
    data['nexturl'] = this._nexturl;
    if (this._addonCat != null) {
      data['addonCat'] = this._addonCat.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AddonCat {
  String _addonCategory;
  String _addonCategoryId;
  int _addonSelection;
  String _nexturl;
  List<Addons> _addons;

  AddonCat(
      {String addonCategory,
        String addonCategoryId,
        int addonSelection,
        String nexturl,
        List<Addons> addons}) {
    this._addonCategory = addonCategory;
    this._addonCategoryId = addonCategoryId;
    this._addonSelection = addonSelection;
    this._nexturl = nexturl;
    this._addons = addons;
  }

  String get addonCategory => _addonCategory;
  set addonCategory(String addonCategory) => _addonCategory = addonCategory;
  String get addonCategoryId => _addonCategoryId;
  set addonCategoryId(String addonCategoryId) =>
      _addonCategoryId = addonCategoryId;
  int get addonSelection => _addonSelection;
  set addonSelection(int addonSelection) => _addonSelection = addonSelection;
  String get nexturl => _nexturl;
  set nexturl(String nexturl) => _nexturl = nexturl;
  List<Addons> get addons => _addons;
  set addons(List<Addons> addons) => _addons = addons;

  AddonCat.fromJson(Map<String, dynamic> json) {
    _addonCategory = json['addon_category'];
    _addonCategoryId = json['addon_category_id'];
    _addonSelection = json['addon_selection'];
    _nexturl = json['nexturl'];
    if (json['addons'] != null) {
      _addons = new List<Addons>();
      json['addons'].forEach((v) {
        _addons.add(new Addons.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addon_category'] = this._addonCategory;
    data['addon_category_id'] = this._addonCategoryId;
    data['addon_selection'] = this._addonSelection;
    data['nexturl'] = this._nexturl;
    if (this._addons != null) {
      data['addons'] = this._addons.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Addons {
  String _dishId;
  String _dishName;
  double _dishPrice;
  String _dishImage;
  String _dishCurrency;
  double _dishCalories;
  String _dishDescription;
  bool _dishAvailability;
  int _dishType;

  Addons(
      {String dishId,
        String dishName,
        double dishPrice,
        String dishImage,
        String dishCurrency,
        double dishCalories,
        String dishDescription,
        bool dishAvailability,
        int dishType}) {
    this._dishId = dishId;
    this._dishName = dishName;
    this._dishPrice = dishPrice;
    this._dishImage = dishImage;
    this._dishCurrency = dishCurrency;
    this._dishCalories = dishCalories;
    this._dishDescription = dishDescription;
    this._dishAvailability = dishAvailability;
    this._dishType = dishType;
  }

  String get dishId => _dishId;
  set dishId(String dishId) => _dishId = dishId;
  String get dishName => _dishName;
  set dishName(String dishName) => _dishName = dishName;
  double get dishPrice => _dishPrice;
  set dishPrice(double dishPrice) => _dishPrice = dishPrice;
  String get dishImage => _dishImage;
  set dishImage(String dishImage) => _dishImage = dishImage;
  String get dishCurrency => _dishCurrency;
  set dishCurrency(String dishCurrency) => _dishCurrency = dishCurrency;
  double get dishCalories => _dishCalories;
  set dishCalories(double dishCalories) => _dishCalories = dishCalories;
  String get dishDescription => _dishDescription;
  set dishDescription(String dishDescription) =>
      _dishDescription = dishDescription;
  bool get dishAvailability => _dishAvailability;
  set dishAvailability(bool dishAvailability) =>
      _dishAvailability = dishAvailability;
  int get dishType => _dishType;
  set dishType(int dishType) => _dishType = dishType;

  Addons.fromJson(Map<String, dynamic> json) {
    _dishId = json['dish_id'];
    _dishName = json['dish_name'];
    _dishPrice = json['dish_price'];
    _dishImage = json['dish_image'];
    _dishCurrency = json['dish_currency'];
    _dishCalories = json['dish_calories'];
    _dishDescription = json['dish_description'];
    _dishAvailability = json['dish_Availability'];
    _dishType = json['dish_Type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dish_id'] = this._dishId;
    data['dish_name'] = this._dishName;
    data['dish_price'] = this._dishPrice;
    data['dish_image'] = this._dishImage;
    data['dish_currency'] = this._dishCurrency;
    data['dish_calories'] = this._dishCalories;
    data['dish_description'] = this._dishDescription;
    data['dish_Availability'] = this._dishAvailability;
    data['dish_Type'] = this._dishType;
    return data;
  }
}