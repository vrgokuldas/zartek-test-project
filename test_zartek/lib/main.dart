import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_zartek/providers/provider_cart.dart';
import 'package:test_zartek/providers/provider_restaurent.dart';
import 'package:test_zartek/screens/screen_homepage.dart';

void main() => runApp(ZarTekTestApp());

class ZarTekTestApp extends StatelessWidget {
  static final appName = 'ZartekTest';

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: ProviderRestaurent()),
        ChangeNotifierProvider.value(value: ProviderCart()),
      ],
      child: MaterialApp(
        title: appName,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.pink,
          primarySwatch: Colors.amber,
        ),
        routes: {
          '/': (ctx) => ScreenHome(),
        },
      ),
    );
  }
}
