import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:test_zartek/models/model_items.dart';
import 'package:test_zartek/models/model_restaurent_pool.dart';
/*
* Controlling the connection between server and data parsing
* */
class ProviderRestaurent with ChangeNotifier {
  List<ModelRestaurent> _restaurentList;
  List<String> _categoriesList = new List<String>();
  List<ModelItems> _optimizedItemsList = new List<ModelItems>();
  String _retaurentName;
  bool isLoading = true;

  ProviderRestaurent() {
    getDataFromServer();
  }

  Future<void> getDataFromServer() async {
    isLoading=true;
    Dio dio = new Dio();
    if (Platform.isAndroid) {
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };
    }
    final url = 'https://www.mocky.io/v2/5dfccffc310000efc8d2c1ad';
    Response response;

    response = await dio.get(url);
    print(response.data.toString());
    _parseDataFromJson(response.data);
    isLoading=false;
    notifyListeners();
  }

  void _parseDataFromJson(List<dynamic> json) {
    ModelRestaurentPool pool = ModelRestaurentPool.fromJson(json);
    _restaurentList = pool.restaurentList;
    _retaurentName = pool.restaurentList[0].restaurantName;
    print(_restaurentList.length);
    print(CategoriesList.length);
    _optimizedItemsList.clear();
    for (TableMenuList menu in pool.restaurentList[0].tableMenuList) {
      for (CategoryDishes dish in menu.categoryDishes) {
        _addToOptimizedList(dish, menu.menuCategory);
      }
    }
  }

  void _addToOptimizedList(CategoryDishes dish, String category) {
    ModelItems items = ModelItems(
        id: dish.dishId,
        menuCategory: category,
        name: dish.dishName,
        price: dish.dishPrice.toString(),
        currencyType: dish.dishCurrency,
        calories: dish.dishCalories.toString(),
        description: dish.dishDescription,
        imageUrl: dish.dishImage,
        isAddonAvailable:
            dish.addonCat != null && dish.addonCat.length > 0 ? true : false,
        foodType: dish.dishType);
    _optimizedItemsList.add(items);
  }

  List<ModelItems> OptimizedFoodList(String category) {

    return _optimizedItemsList
        .where((food) => food.menuCategory == category)
        .toList();
  }

  List<String> get CategoriesList {
    _categoriesList.clear();
    _restaurentList[0]
        .tableMenuList
        .map((menu) => _categoriesList.add(menu.menuCategory))
        .toList();
    return [..._categoriesList];
  }

  String get RestaurentName {
    return _retaurentName;
  }
}
