import 'package:flutter/material.dart';
import 'package:test_zartek/models/model_cart_item.dart';
import 'package:test_zartek/models/model_items.dart';
/*
* Controlling cart inside the app
*/
class ProviderCart with ChangeNotifier{
  Map<String, ModelCartItem> _items={};

  int get ItemCount
  {
    return _items == null ? 0 : _items.length;
  }
  /*
  * Function to add items to cart
  * */
  void addItem(ModelItems newItem) {
    if (_items.containsKey(newItem.id)) {
      _items.update(
          newItem.id,
              (oldItem) =>
              ModelCartItem(item: newItem, quantity: oldItem.quantity + 1));
    } else {
      _items.putIfAbsent(
          newItem.id, () => ModelCartItem(item: newItem, quantity: 1));
    }
    notifyListeners();
  }
/*
  * Function to remove item from cart
  * */
  void removeSingleItem(String id)
  {
    if(_items.containsKey(id))
    {
      if(_items[id].quantity==1)
      {
        _items.remove(id);
      }
      else{
        _items.update(id, (oldItem)=>ModelCartItem(item: oldItem.item,quantity: oldItem.quantity-1));
      }
      notifyListeners();
    }
    else{
      print('cart item with id not found');
    }
  }
/*
  * Function to get the count of items from cart.If item not found, it return ZERO
  * */
  int getItemCountFromCart(String id)
  {
    return _items.containsKey(id)?_items[id].quantity :0;
  }

}